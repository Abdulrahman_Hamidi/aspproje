﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="st.aspx.cs" Inherits="hm.st" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:StorConnectionString %>" SelectCommand="SELECT * FROM [User]" InsertCommand="INSERT INTO [User] (name, email) VALUES (@name,@email)" UpdateCommand="UPDATE [User] SET name =@name, email =@email WHERE id =@id ;">
            <InsertParameters>
                <asp:ControlParameter ControlID="name" Name="name" PropertyName="Text" />
                <asp:ControlParameter ControlID="email" Name="email" PropertyName="Text" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="id" Name="id" PropertyName="Text" />
                <asp:ControlParameter ControlID="name" Name="name" PropertyName="Text" />
                <asp:ControlParameter ControlID="email" Name="email" PropertyName="Text" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:Button ID="Btn" runat="server" Text="save" OnClick="Btn_Click" />
        <asp:Button ID="btn2" runat="server" Text="update" OnClick="btn2_Click" />
        <asp:TextBox ID="name" runat="server"></asp:TextBox>
        <asp:TextBox ID="email" runat="server"></asp:TextBox>
    
        <asp:TextBox ID="id" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
    
    </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
                <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
                <asp:BoundField DataField="nickname" HeaderText="nickname" SortExpression="nickname" />
                <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                <asp:BoundField DataField="phone" HeaderText="phone" SortExpression="phone" />
                <asp:BoundField DataField="adress1" HeaderText="adress1" SortExpression="adress1" />
                <asp:BoundField DataField="adress2" HeaderText="adress2" SortExpression="adress2" />
            </Columns>
        </asp:GridView>
    </form>
</body>
</html>
