﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(hm.Startup))]
namespace hm
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
