﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using hm.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace hm
{
    public partial class profile : System.Web.UI.Page
    {

        protected List<product> GetPeoducts(int sellerID)
        {
            finalEntities context = new finalEntities();
            List<product> pro = context.products.Where(p => p.sellerID == sellerID).ToList();

            return pro;
        }

        protected List<product> GetPeoducts(int sellerID,int orders)
        {
            finalEntities context = new finalEntities();
            List<product> pro = context.products.Where(p => p.sellerID == sellerID).ToList();

            return pro;
        }



        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userID"]==null)
            {
                Response.Redirect("home.aspx");
            }
            
        }
        void getValues()
        {
            int id = Int32.Parse( Session["userID"].ToString());//Int32.Parse(Session["userID"].ToString());
            finalEntities context = new finalEntities();
            user us = context.users.Single(u => u.id == id);
            name.Text = us.name;
            surename.Text = us.surname;
            username.Text = us.username;
            email.Text = us.email;
            phone.Text = us.phone.ToString() == null ? "" : us.phone.ToString();
            adress.Text = us.adress1;
        }

        protected void edit_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(Session["userID"].ToString());

            if (name.ReadOnly)
            {
                getValues();
                name.ReadOnly = false;
                surename.ReadOnly = false;
                username.ReadOnly = false;
                email.ReadOnly = false;
                phone.ReadOnly = false;
                adress.ReadOnly = false;

            }else if ( name.Text!=null || surename !=null || phone !=null || adress !=null || email.Text !=null || username.Text !=null)
            {
                string cs = ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
                string sql = "UPDATE [dbo].[user]SET [name] = '"+name.Text+"',[surname] = '"+surename.Text+"',[email] = '"+email.Text+"',[phone] = '"+ phone.Text+"',[adress1] = '"+adress.Text+"',[username] = '"+username.Text+"'WHERE id = "+id;
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand(sql, con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

    }
}