﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="profile.aspx.cs" Inherits="hm.profile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="styles/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="styles/main.css" rel="stylesheet" />
    <link href="styles/meidaQueries/style1200.css" rel="stylesheet" />
    <link href="styles/meidaQueries/style800.css" rel="stylesheet" />
    <link href="styles/meidaQueries/style620.css" rel="stylesheet" />
    <link href="styles/meidaQueries/style480.css" rel="stylesheet" />
    <link href="styles/font-awesome/css/font-awesome.css" rel="stylesheet" />
     <script src="Scripts/jquery-3.1.1.js"></script>
    <script src="Scripts/bootstrap.js"></script>
    <style >


        .top-sec{
            min-width:220px;
        }
        .body > div:nth-child(2n+1):not(:first-child){
            background-color:#00283B;
            text-align:left;
            color:#ffffff;
        }
        .body{
            width:100%;
        }
        .row{
            margin:0;
        }
        input{
            margin-bottom:10px;
            outline:none !important; 
            
            box-shadow:0px 2px 10px 0px rgba(0, 0, 0, 0.2) !important;
            
            padding:5px;
        }
        input:focus{
             box-shadow:0px 17px 15px -10px rgba(0, 0, 0, 0.4) !important;
 
}
        #edit{
            width:100%;
            height:40px;
            background-color:#247884;
            color:#ffffff;
            font-size:20px;
            padding:5px;
            border:1px solid #b4a96d;
            border-radius:4px;
        }

        .add{
            text-decoration:none;
            color:#000;
            font-size:22px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="width:100%;">
        <%if (Session["userID"]==null)
            {
                Session["warning"] = "you have to login to open this page";
                //Response.Redirect("home.aspx");
            } %>
    <div class="body">
        <!--navbar-->
      <nav class="navbar navbar-default navbar-fixed-top" style="background-color:#247884;">
          <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>                        
              </button>
              <a class="navbar-brand" href="home.aspx">H&M</a>
            </div>
            <div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                  <li><a href="#info">My information</a></li>
                  <li><a href="#orders">My orders </a></li>
                  <li><a href="#products">My products</a></li>
                 
                </ul>
              </div>
            </div>
          </div>
        </nav>    

        <!--body-->
        <!--my information sec in the body-->
        <div id="info" class="container-fluid">
          <h1>My information</h1>
            <div class="row" style="text-align:left; padding:10px 20px 30px 20px;">
           
                    <div class="col-md-6">
                        <label for="name">Name</label>
                        <asp:TextBox ID="name" runat="server" ReadOnly CssClass="form-control" ></asp:TextBox>

                         <label for="surename">Surename</label>
                        <asp:TextBox ID="surename" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>

                         <label for="username">Username</label>
                        <asp:TextBox ID="username" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                         <label for="adress">Adress</label>
                         <asp:TextBox ID="adress" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>

                         <label for="phone">Phone</label>
                         <asp:TextBox ID="phone" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>

                         <label for="email">E-mail</label>
                         <asp:TextBox ID="email" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>
                    </div>
                   <div class="col-md-6" style="float:right;">
                       <asp:Button ID="edit" runat="server" Text="Edit" OnClick="edit_Click"/>
                    </div>
                   
                
            </div>
        </div>
        <!--my orders sec in the body-->
        <div id="orders" class="container-fluid">
          <h1>My orders</h1>
             <div class="row" style="background-color:#00283B;">

            </div>
        </div>
        <!--my products sec in the body-->
        <div id="products" class="container-fluid">
          <h1>My products</h1> <a href="product/addProduct" class="add">Add Product</a>
             <div class="row" style="padding:20px">
                  <% foreach (var item in GetPeoducts(Int32.Parse( Session["userID"].ToString())))
                    {%>
                    <div class="col-sm-5 col-md-3 white  top-sec margin-10">
                        
                        <img src="<%Response.Write( item.image.ToString()); %>" />
                        <div class="left-side">
                            <div class="product-name">
                               <a href="product/Details/<%Response.Write(item.id.ToString()); %>"><%Response.Write( item.name.ToString()); %></a>
                            </div>
                                <br />
                            <div class="price">
                               <a href="product/Edit/<%Response.Write( item.id.ToString()); %>">Edit</a>
                            </div>

                            
                          
                        </div>
                    </div>
               <% }%>
            </div>

        </div>
        <!--footer-->

        <footer style="text-align:center">

            <div class="row" style="margin:0px;">
                <div class="col-lg-12 brand-footer">H&M</div>
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="col-lg-1 col-lg-offset-3"><a>US</a></div>
                    <div class="col-lg-1"><a href="#">UK</a></div>
                    <div class="col-lg-1"><a href="#">KSA</a></div>
                    <div class="col-lg-1"><a href="#">TR</a></div>
                    <div class="col-lg-1"><a href="#">UAE</a></div>
                    <div class="col-lg-1"><a href="#">SY</a></div>
                </div>
            </div>
            <p>&copy; <%Response.Write(DateTime.Now.Year);%> - H&M </p> &nbsp; &nbsp; &nbsp;
            <p>Made with <i style="color:red; font-size:20px;" class="fa fa-heart-o" aria-hidden="true"></i> By: Abdulrahman Hamidi</p>
        </footer>
    </div>
    </form>
</body>
</html>
