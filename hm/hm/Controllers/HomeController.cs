﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hm.Models;
namespace hm.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //finalEntities context = new finalEntities();
            //List<category> c= context.categories.ToList();
            return View();
        }


        public void logout()
        {
            Session.Remove("userID");
            Session.Remove("userName");
            Response.Redirect("~/home.aspx");
        }
        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}