﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hm.Models;
using System.Data.SqlClient;
using System.Configuration;
namespace hm.Controllers
{
    public class ProductController : Controller
    {
        int id;
        // GET: Product
        public ActionResult Details(int ID)
        {
           
            ViewData["id"] = ID;
            finalEntities context = new finalEntities();
            product pro = context.products.Single(p => p.id == ID);
            ViewBag.stars = "";
            foreach (var item in GetStars(pro.id))
            {
                ViewBag.stars = ViewBag.stars += item;
            }
            //ViewBag.stars = GetStars(pro.id).ToList();
            return View(pro);
           
        }

        protected string[] GetStars(int? rate)
        {
            string[] star = { "", "", "", "", "" };
            switch (rate)
            {
                case 0:
                    star[0] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 1:
                    star[0] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 2:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 3:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 4:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 5:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 6:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 7:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 8:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 9:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    break;
                case 10:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    break;
                default:
                    goto case 0;
                    break;


            }
            return star;
        }


        public PartialViewResult GetComments(int id)
        {
            finalEntities context = new finalEntities();
            List<comment> com = context.comments.Where(c => c.porductID == id).ToList();
           
            return PartialView ("_comments",com);
        }

        [HttpGet]
        public ActionResult WriteComments(int ID)
        {
            comment co = new comment();
            co.porductID = ID;
            return PartialView("_WriteComments",co);
        }

        [HttpPost]
        public void WriteComments(FormCollection col)
        {
            id =Int32.Parse( col["porductID"]);
            comment c = new comment();
            c.comment1 = col["comment1"];
            c.date = DateTime.Now;
            c.commenter = Session["userName"].ToString();
            AddComment(col["comment1"],id);
            Response.Redirect("../../product/Details/" + id);

            //return PartialView("_WriteComments");
        }

        public void AddComment(string comm,int id)
        {

            DateTime d = DateTime.Now;
            string name = Session["userName"].ToString();
            string sql = "INSERT INTO [dbo].[comments]([comment],[date] ,[commenter],[porductID])VALUES('"+comm+"','"+d+"','"+name+"','"+id+"')";
            string cs = ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(sql,con);
                cmd.ExecuteNonQuery();
            }
           
        }

        [HttpGet]
        public ActionResult Edit(int ID)
        {
            finalEntities context = new finalEntities();
            product p= context.products.Single(pp => pp.id == ID);

           

            return View(p);
        }
        [HttpPost]
        public void Edit(FormCollection col)
        {

            edit(col);
            Response.Redirect("../../profile.aspx");
            //return View();
        }

        public void edit(FormCollection col)
        {
            string cs = ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            string sql = "UPDATE [dbo].[product]SET [name] = '" + col["name"] + "' ,[stack] = '" + col["stack"] + "',[discription] ='" + col["discription"] + "' ,[price] = " + col["price"] + "  WHERE id=1";
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
            }

            
        }

        public ActionResult addProduct()
        {
            return View();
        }
        [HttpPost]
        public ActionResult addProduct(FormCollection col)
        {
            string cs = ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            string sql = "INSERT INTO [dbo].[product]([name],[stack],[discription] ,[price],[sellerID] ,[UploadDate],[image],[rate],[raters]) VALUES ('" + col["name"]+ "','" + col["stack"] + "','" + col["discription"] + "','" + col["price"] + "','"+Session["userID"]+"','"+DateTime.Now+ "','photos/nikonCamera.jpg','5','34524')";
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
            }
            return View();
        }


    }
}