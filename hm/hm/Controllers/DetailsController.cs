﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hm.Models;
namespace hm.Controllers
{
    public class DetailsController : Controller
    {
        // GET: Details
        public ActionResult Details(int ID)
        {

            finalEntities context = new finalEntities();
            product pro = context.products.Single(p => p.id == ID);
            return View(pro);
        }

        

        public product getProduct(int ID)
        {
            finalEntities context = new finalEntities();
            product pro= context.products.Single(p => p.id == ID);
            return pro;
        }
        
    }
}