﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.UI;

namespace hm
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //RouteConfig.RegisterRoutes(System.Web.Routing.RouteTable.Routes);
            //ScriptManager.ScriptResourceMapping.AddDefinition("jquery",
            //    new ScriptResourceDefinition
            //    {
            //        Path = "/~Scripts/jquery-1.10.2.min.js"
            //    }
            //);
        }
    }
}
