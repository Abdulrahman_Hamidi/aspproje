﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="hm.home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        
    </style>
    
<div class="row" style="margin:0;">


    <div class="col-sm-12 title-text" style="display:block;">Recommended For You </div>
   
    <% foreach (var item in GetPeoducts())
        {%>
        <div class="col-sm-5 col-md-3 white  top-sec margin-10">

            <img src="<%Response.Write( item.image.ToString()); %>" />
            <div class="left-side">
                <div class="product-name">
                   <a href="product/Details/<%Response.Write(item.id.ToString()); %>"><%Response.Write( item.name.ToString()); %></a>
                </div>
                <div class="by">
                    <a href="user/review/<%Response.Write(item.sellerID); %>">By:<%Response.Write(getSellerName(Int32.Parse( item.sellerID.ToString()))); %></a>

                </div><br />
                <div class="price">
                    <span class="para">$</span><span class="price-tl"><%Response.Write(item.price.ToString()); %></span><span class="price-kr"></span>
                </div>

                <div class="rate">
                        <%for (int i = 0; i < 5; i++)
                            { 
                                Response.Write( GetStars(item.rate)[i].ToString());
                            } %>
                   
                </div>
                <div class="rate-number">(<%Response.Write(item.raters.ToString()); %>)</div>
            </div>
        </div>
   <% }%>



</div>

</asp:Content>
