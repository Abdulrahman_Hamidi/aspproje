﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using hm.Models;

namespace hm
{
    public partial class login : System.Web.UI.Page
    {
        string username= "";
        int UserID;
        protected void Page_Load(object sender, EventArgs e)
        {
           

            Password.TextMode = TextBoxMode.Password;
            Email.TextMode = TextBoxMode.Email;
        }
        public bool getUser(string email,string Password)
        {
            finalEntities context = new finalEntities();

            List<user> usr = context.users.ToList();
            bool exist = false;
            foreach (var item in usr)
            {

                if (item.email == email && item.password == Password)
                {
                    exist = true;
                    username = item.username;
                    UserID = item.id;
                    break;
                }
                else
                {
                    exist = false;
                }
            }

            return exist;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (getUser(Email.Text , Password.Text))
            {
                Session["username"] = username;
                Session["userID"] = UserID;
                Response.Redirect("home.aspx");
            }else
            {
                varr.Text = "Please check the information that you have entered and try again.";
            }
        }



    }
}