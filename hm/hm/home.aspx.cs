﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using hm.Models;

namespace hm
{
    public partial class home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected List<product> GetPeoducts()
        {
            finalEntities context = new finalEntities();
            List<product> pro = context.products.ToList();

            return pro;
        }
        protected List<product> GetPeoducts(int sellerID)
        {
            finalEntities context = new finalEntities();
            List<product> pro = context.products.Where(p => p.sellerID ==sellerID).ToList();

            return pro;
        }

        public string getSellerName(int ID)
        {
            finalEntities contex = new finalEntities();
            user uu= contex.users.Single(u => u.id == ID);
            if (uu.username.Length>12)
            {
                string name = uu.username.Substring(0, 13) + "...";
                return name;
            }
            return uu.username;
        }

       

        protected string[] GetStars(int? rate)
        {
            string[] star = { "","","","",""};
            switch (rate)
            {
                case 0:
                    star[0] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 1:
                    star[0] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 2:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 3:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 4:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 5:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 6:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 7:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 8:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-o' aria-hidden='true'></i></span>";
                    break;
                case 9:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star-half-o' aria-hidden='true'></i></span>";
                    break;
                case 10:
                    star[0] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[1] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[2] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[3] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    star[4] = "<span><i class='fa fa-star' aria-hidden='true'></i></span>";
                    break;
                default:
                    goto case 0;
                    break;
                
               
            }
            return star;
        }
    }
}