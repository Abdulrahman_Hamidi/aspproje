﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="hm.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .log-form{
            margin-top:70px;
            width:100%;
            height:100%;
           
            display:inline-block;
        }
        .form input{
            width:50%;
        }
        html,body{
            height:100%;
            text-align:center;
        }
        .categories,.categories-trigger{
            display:none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div></div>
    <div class="log-form">
    
        <form runat="server" style="width:100%;">
            <label for="Email">E-mail</label><br />
            <asp:TextBox ID="Email" runat="server" CssClass="input"></asp:TextBox><br /><br />
            <label for="Password">Password</label><br />
            <asp:TextBox ID="Password" runat="server" CssClass="input"></asp:TextBox><br /><br />
            <asp:Button ID="Button1" runat="server" Text="Login" UseSubmitBehavior="true" OnClick="Button1_Click" CssClass="sbmt-btn"/> <br />
            <asp:Label ID="varr" runat="server" Text=""></asp:Label>
        </form>
    </div>
       
</asp:Content>
