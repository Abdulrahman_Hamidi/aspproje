﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using hm.Models;
using System.Configuration;
using System.Data.SqlClient;


namespace hm
{
    public partial class Register : System.Web.UI.Page
    {
        List<string> errors = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            password.TextMode = TextBoxMode.Password;
            email.TextMode = TextBoxMode.Email;
        }

        protected void submit(object sender, EventArgs e)
        {
            varr.Text = "";
            if (username.Text == null || email.Text == null || password.Text == null)
            {
                errors.Add("please fill All fields !");
            }
            else if (password.Text.Length<8)
            {
                errors.Add("Your password must be more than 8 letters");
            } 
            else if(getUser(email.Text))
            {
                errors.Add("This Email Is User Before ! Please use another email.");
            }else
            {
                string cs = ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
                using(SqlConnection conn = new SqlConnection(cs))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO [dbo].[user] ([email],[password],[username]) VALUES('"+email.Text+"','"+password.Text+"','"+username.Text+"')", conn);
                    cmd.ExecuteNonQuery();
                }
                varr.Text = "Done ! Do you want to " + " <a href='login.aspx?email="+email.Text+"'>log in</a>";
                foreach (var item in Page.Controls)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).Text = string.Empty;
                    }
                }
            }


            foreach (var item in errors)
            {
                varr.Text = varr.Text + item + "<br>"; 
            }
            
        }
     


        public bool getUser(string email)
        {
            finalEntities context = new finalEntities();

            List<user> usr =  context.users.ToList();
            bool exist = false;
            foreach (var item in usr)
            {

                if (item.email == email)
                {
                    exist = true;
                    break;
                }
                else
                {
                    exist = false;
                }
            }

            return exist;

        }
    }
}